class RabbitsController < ApplicationController
  def new
    check_if_user_has_a_rabbit
  end
  def create
    puts params.inspect
    rabbit = Rabbit.new(rabbit_params)
    rabbit.user = current_user
    if rabbit.save
      head :created
    else
      head :bad_request
    end
  end
  def show
    check_if_user_has_not_a_rabbit
    @rabbit = Rabbit.find(params[:id])
  end

  private
  def rabbit_params
    rabbit_params = params.permit(:name, :fur_color, :kind)
  end
  def check_if_user_has_a_rabbit
    if current_user.rabbit
      redirect_to action: 'show', id: current_user.rabbit and return
    end
  end
  def check_if_user_has_not_a_rabbit
    unless current_user.rabbit
      redirect_to action: 'new' and return
    end
  end
end
