class Rabbit < ApplicationRecord

  belongs_to :user

  enum fur_color: [:white, :black, :gray, :orange, :yellow, :pink]
  enum kind: [:wizard, :sorcerer, :warrior, :knight, :hunter, :assassin, :priest, :healer]

  validates :name, presence: true
  validates :fur_color, presence: true, inclusion: { in: Rabbit.fur_colors.keys }
  validates :kind, presence: true, inclusion: { in: Rabbit.kinds.keys }

  def fur_color=(value)
    self[:fur_color] = value
  rescue ArgumentError
    self[:fur_color] = nil
  end

  def kind=(value)
    self[:kind] = value
  rescue ArgumentError
    self[:kind] = nil
  end

end
