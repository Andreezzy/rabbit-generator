import React from "react";
import { render } from "react-dom";
import App from "./rabbit_form/App";

document.addEventListener("DOMContentLoaded", () => {
  render(<App />, document.getElementById('app'));
});