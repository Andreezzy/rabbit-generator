import React, { useState } from 'react';
import LoadingImage from '../../images/loading.gif';

function App() {
  const questions = [{ label: 'Name', name: 'name', options: null },
                      { label: 'Fur Color', name: 'fur_color', options: ['white', 'black', 'gray', 'orange', 'yellow', 'pink'] },
                      { label: 'Class', name: 'kind', options: ['wizard', 'sorcerer', 'warrior', 'knight', 'hunter', 'assassin', 'priest', 'healer'] }];

  const [step, setStep] = useState(0);
  const [payload, setPayload] = useState({ name: '', fur_color: '', kind: '' });

  const handleSubmit = (evt) => {
    setStep(step + 1);
    if(step >= questions.length - 1){
      const token = document.querySelector('[name=csrf-token]').content;
      const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'X-CSRF-TOKEN': token},
        body: JSON.stringify(payload)
      };
      fetch('/rabbits', requestOptions)
        .then(response => {
          if (response.status == 201) {
            window.location.href = '/';
          }else{
            setStep(0);
          }
        })
        .catch(error => {
          setStep(0);
        });
    }
    evt.target[0].value = '';
    evt.preventDefault();
  }

  const handleSetPayload = (e) => {
    e.target.value = e.target.value.replace(/[^a-z0-9]/gi,'');
    setPayload({ ...payload, [questions[step].name]: e.target.value });
  }

  return (
    <form onSubmit={ handleSubmit }>
      <h2>Create a Rabbit</h2>
      {
        (step < questions.length)
          ?
            <>
              <div className='flex flex-col field'>
                <label>{questions[step].label}:</label>
                {
                  questions[step].options == null
                    ?
                    <input className='input-text form-input' onChange={ (e) => handleSetPayload(e) } />
                    :
                    <select className='select-tag capitalize' onChange={(e) => setPayload({ ...payload, [questions[step].name]: e.target.value })}>
                      <option value=''></option>
                      {
                        questions[step].options.map((option, i) => (
                          <option key={i} value={option}>
                            { option }
                          </option>
                        ))
                      }
                    </select>
                }
              </div>
              <div className='actions'>
                <button className={`btn-primary mt-4 ${payload[questions[step].name].length < 3 ? 'bg-opacity-25': ''}`} disabled={payload[questions[step].name].length < 3}>
                  { step >= questions.length - 1 ? 'Submit' : 'Next'}
                </button>
              </div>
            </>
          :
          <img src={LoadingImage} />
      }
    </form>
  );
}

export default App;
