# README

Things you may want to cover:
* Ruby version: ruby '3.0.1'
  
* Rails version: gem 'rails', '~> 6.1.3', '>= 6.1.3.2'

* Database: sqlite3 for dev and test, pg for prod

Improvements that I did not make due to lack of time:
* React code refactor
* Dynamic labels in the rabbits form, currently they are written in the same js file and it is not bringing it from the same Rabbit model
* Rabbit model code refactor
* More validations in Rabbit attributes