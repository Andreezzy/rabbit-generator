class CreateRabbits < ActiveRecord::Migration[6.1]
  def change
    create_table :rabbits do |t|
      t.string :name, null: false
      t.integer :fur_color, null: false
      t.integer :kind, null: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
