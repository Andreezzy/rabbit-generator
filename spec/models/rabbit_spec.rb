require 'rails_helper'

RSpec.describe Rabbit, type: :model do

  let(:rabbit) { create(:rabbit) }
  it "is valid with valid attributes" do
    expect(rabbit).to be_valid
  end

  describe '#name' do
    it 'is required' do
      rabbit.name = nil
      rabbit.valid?
      expect(rabbit.errors[:name].size).to eq(1)
    end
  end

  describe '#fur_color' do
    it 'is required' do
      rabbit.fur_color = nil
      rabbit.valid?
      expect(rabbit.errors[:fur_color].size).to eq(2)
    end

    let(:fur_colors) { [:white, :black, :gray, :orange, :yellow, :pink] }
    it 'is a valid fur color' do
      fur_colors.each_with_index do |item, index|
        expect(Rabbit.fur_colors[item]).to eq(index)
      end
    end

    it 'is not a valid fur color' do
      rabbit.fur_color = fur_colors.size
      rabbit.valid?
      expect(rabbit.errors[:fur_color].size).to eq(2)
    end
  end

  describe '#kind' do
    it 'is required' do
      rabbit.kind = nil
      rabbit.valid?
      expect(rabbit.errors[:kind].size).to eq(2)
    end

    let(:kinds) { [:wizard, :sorcerer, :warrior, :knight, :hunter, :assassin, :priest, :healer] }
    it 'is a valid kind' do
      kinds.each_with_index do |item, index|
        expect(Rabbit.kinds[item]).to eq(index)
      end
    end

    it 'is not a valid kind' do
      rabbit.kind = kinds.size
      rabbit.valid?
      expect(rabbit.errors[:kind].size).to eq(2)
    end
  end

end
