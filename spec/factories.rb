FactoryBot.define do
  factory :user do
    name { "Andres" }
    email { "andres@user.com" }
    password { "qwerty" }
    password_confirmation { 'qwerty' }
    confirmed_at { Time.now.utc }
  end

  factory :rabbit do
    name { "Morphy" }
    fur_color { "white" }
    kind { "wizard" }
    user
  end
end