require 'rails_helper'

RSpec.describe "Rabbits", type: :request do

  let(:user) { create(:user) }

  describe "GET /new without login" do
    it "redirects to login template" do
      get "/rabbits/new"
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  describe "GET /new with login" do
    before(:each) do
      sign_in user
      get "/rabbits/new"
    end

    it "renders rabbits/new template" do
      expect(response.body).to include("app")
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST /create with login" do
    before(:each) do
      sign_in user
      @rabbit = attributes_for(:rabbit)
    end
    it "returns http success 201" do
      post "/rabbits", params: @rabbit
      expect(response).to have_http_status(201)
    end
    it "returns http error 400" do
      @rabbit["name"] = nil
      post "/rabbits", params: @rabbit
      expect(response).to have_http_status(400)
    end
  end

  describe "POST /create without login" do
    it "redirects to login template" do
      @rabbit = attributes_for(:rabbit)
      post "/rabbits", params: @rabbit
      expect(response).to redirect_to(new_user_session_path)
    end
  end

end
