require 'rails_helper'

RSpec.describe "Users", type: :request do

  let(:user) { create(:user) }

  describe "GET /users/sign_in" do
    context "User without a rabbit" do
      it "should get status 200 for rabbits#new after logged in" do
        sign_in user
        get "/"
        expect(response).to have_http_status(200)
      end
    end
    context "User with a rabbit" do
      it "should redirect to rabbits#show after logged in" do
        rabbit = create(:rabbit, { user: user })
        sign_in user
        get "/"
        expect(response).to redirect_to("/rabbits/#{rabbit.id}")
      end
    end
  end
end