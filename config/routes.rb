Rails.application.routes.draw do
  resources :rabbits, only: [:new, :create, :show]
  devise_for :users
  root 'rabbits#new'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
